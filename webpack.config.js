const path = require('path');
const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

module.exports = Encore
    // The directory where your files should be output.
    .setOutputPath('public/build')
    // The public version of outputPath: the public path to outputPath.
    .setPublicPath('/build')

    // Adds a JavaScript file that should be webpacked.
    .addEntry('app', './assets/js/app.js')

    // When enabled, files are rendered with a hash based on their contents.
    .enableVersioning(Encore.isProduction())
    // When enabled, all final CSS and JS files will be rendered with sourcemaps
    // to help debugging.
    .enableSourceMaps(Encore.isDev())

    // Copy files or folders to the build directory.
    //.copyFiles({ })

    // Tell Webpack to output a separate runtime.js file.
    .enableSingleRuntimeChunk()
    // Tell Webpack to "split" your entry chunks.
    .splitEntryChunks()

    // Call this if you plan on loading SASS files.
    .enableSassLoader()
    // If enabled, the react preset is added to Babel.
    .enableReactPreset()
    // If enabled, the eslint-loader is enabled.
    .enableEslintLoader()

    // If enabled, the output directory is emptied between each build
    // (to remove old files).
    .cleanupOutputBeforeBuild()
    // If enabled, add integrity hashes to the entrypoints.json file for all
    // the files it references.
    .enableIntegrityHashes(Encore.isProduction())

    // Get configured webpack configuration.
    .getWebpackConfig()
;
