<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * Show homepage of the application.
     *
     * @Route("/", name="homepage")
     * @Route("/explore/{categoryId}")
     */
    public function index(): Response
    {
        return $this->render('homepage/index.html.twig');
    }
}
