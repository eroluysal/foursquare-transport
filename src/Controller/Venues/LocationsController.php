<?php

namespace App\Controller\Venues;

use App\Foursquare\Transport\VenueTransport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocationsController extends AbstractController
{
    /**
     * Show locations of categories page of the application.
     *
     * @return Response
     *
     * @Route("/venues/{categoryId}/locations", name="venues_locations")
     */
    public function index(string $categoryId, VenueTransport $venueTransport)
    {
        return $this->json($venueTransport->explore(null, 'valletta', [
            'categoryId' => $categoryId,
        ]));
    }
}
