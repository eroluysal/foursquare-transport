<?php

namespace App\Controller\Venues;

use App\Foursquare\Transport\VenueTransport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController
{
    /**
     * Show venues categories page of the application.
     *
     * @return Response
     *
     * @Route("/venues/categories", name="venues_categories")
     */
    public function index(VenueTransport $venueTransport)
    {
        return $this->json($venueTransport->categories());
    }
}
