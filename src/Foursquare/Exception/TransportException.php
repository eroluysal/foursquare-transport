<?php

namespace App\Foursquare\Exception;

class TransportException extends \RuntimeException
{
    public function __construct(string $type, string $message, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct(sprintf('%s: %s', $type, $message), $code, $previous);
    }
}
