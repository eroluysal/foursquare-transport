<?php

namespace App\Foursquare\Transport;

use App\Foursquare\Client;
use League\Uri\Uri;
use League\Uri\UriModifier;

abstract class AbstractTransport
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Builds new query for given options.
     */
    protected function buildQueryUrl(string $path, array $queries = []): string
    {
        $uri = Uri::createFromString($path);

        foreach (array_filter($queries) as $key => $value) {
            $uri = UriModifier::appendQuery($uri, sprintf('%s=%s', $key, $value));
        }

        return (string) $uri;
    }
}
