<?php

namespace App\Foursquare\Transport;

class VenueTransport extends AbstractTransport
{
    /**
     * Returns a hierarchical list of categories applied to venues. This list
     * is also available on our categories page.
     */
    public function categories()
    {
        return $this->client->request('GET', 'venues/categories');
    }

    /**
     * Returns a list of recommended venues near the current location. For more
     * robust information about the venues themselves (photos/tips/etc.),
     * please see our venue details endpoint.
     *
     * If authenticated, the method will personalize the ranking based on you
     * and your friends.
     *
     * @throws \InvalidArgumentException
     */
    public function explore(string $ll = null, string $near = null, array $options = [])
    {
        if (null === $ll && null === $near) {
            throw new \InvalidArgumentException('ll or near must be provided.');
        }

        $options = array_merge(compact('ll', 'near'), $options);

        return $this->client->request('GET', $this->buildQueryUrl('venues/explore', $options));
    }

    /**
     * Gives the full details about a venue including location, tips, and
     * categories.
     *
     * If the venue ID given is one that has been merged into another venue,
     * the response will show data about the other venue instead of giving you
     * an error.
     *
     * @throws \UnexpectedValueException
     */
    public function get(string $id): array
    {
        return $this->client->request('GET', sprintf('venues/%s', $id));
    }

    /**
     * Returns a list of venues near the current location, optionally matching
     * a search term.
     *
     * To ensure the best possible results, pay attention to the intent
     * parameter below.
     *
     * Note that most of the fields returned inside a venue can be optional.
     * The user may create a venue that has no address, city, or state
     * (the venue is created instead at the lat/long specified).
     *
     * Your client should handle these conditions safely. For more robust venue
     * information (photos/tips/etc.), please see our venue details endpoint.
     *
     * @throws \InvalidArgumentException
     */
    public function search(string $ll = null, string $near = null, array $options = []): array
    {
        if (null === $ll && null === $near) {
            throw new \InvalidArgumentException('ll or near must be provided.');
        }

        $options = array_merge(compact('ll', 'near'), $options);

        return $this->client->request('GET', $this->buildQueryUrl('venues/search', $options));
    }
}
