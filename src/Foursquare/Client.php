<?php

namespace App\Foursquare;

use App\Foursquare\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Client
{
    const BASE_URL = 'https://api.foursquare.com/v2/';

    private $httpClient;
    private $clientId;
    private $secret;

    public function __construct(string $clientId, string $secret, HttpClientInterface $httpClient = null)
    {
        $httpClient = $httpClient ?: HttpClient::create();

        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->httpClient = $httpClient;
    }

    public function request(string $method, string $url, array $options = []): array
    {
        $options = array_merge($this->getDefaultOptions(), $options);

        try {
            $resp = $this->httpClient->request($method, $url, $options)->toArray();
        } catch (HttpExceptionInterface $e) {
            $resp = $e->getResponse()->toArray(false);
            $meta = $resp['meta'];

            throw new TransportException($meta['errorType'], $meta['errorDetail'], $meta['code'], $e);
        }

        return $resp['response'];
    }

    private function getDefaultOptions()
    {
        return (new HttpOptions())
            ->setBaseUri(self::BASE_URL)
            ->setQuery([
                'client_id' => $this->clientId,
                'client_secret' => $this->secret,
                'v' => (new \DateTime())->format('Ymd'),
            ])
            ->toArray()
        ;
    }
}
